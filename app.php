<?php
include("XMPP.php");

try {
    $conn = new XMPPHP_XMPP('talk.google.com', 5222, '[email]', '[password]', 'xmpphp', 'gmail.com', false, XMPPHP_Log::LEVEL_VERBOSE);

    $conn->connect();
    $conn->processUntil('session_start');
    $conn->message('[receiver]', 'This is a test message!');
    # This is where causes the infinite loop where the socket is null for some reason and the stream could be closed.
    $conn->disconnect();
} catch(XMPPHP_Exception $e) {
    return;
} catch(Exception $e) {
    $this->fail('Unexpected Exception thrown: '.$e->getMessage());
}