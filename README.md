XMPP_PHP Sample App
=======

This app is built upon [https://code.google.com/p/xmpphp/](https://code.google.com/p/xmpphp/). To setup the php + apache environment, follow the instructions from this website: [http://www.coolestguyplanettech.com/downtown/install-and-configure-apache-mysql-php-and-phpmyadmin-osx-108-mountain-lion](http://www.coolestguyplanettech.com/downtown/install-and-configure-apache-mysql-php-and-phpmyadmin-osx-108-mountain-lion).

The main function resides in `app.php`. You will need to replace the info wrapped in [] to make it work.

If you follow the setup above and httpd / apahce is started, you can browse to app.php to send messages. However, the messages can be sent through, but an infinite error related to connection closing is also triggered. It seems like the socket is null, but the cause is still unknown.